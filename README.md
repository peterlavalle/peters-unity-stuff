
unity packages i keep re-using ... or trying to not-copy-and-paste ...

- [PostProcessing Graph](post-graph.unity/)
	- build post-processing chains in the Unity Visual Ediutor (not shaders - we needed more power)
	- package URL; `"com.peterlavalle.postgraph": "https://gitlab.com/peterlavalle/peters-unity-stuff.git?path=/post-graph.unity/Packages/com.peterlavalle.postgraph"`

- [Nug](workspace-nug.unity/)
	- grab `.dll` from NuGet without comming them to git 
	- Mercurial users (like me) can chew their own food
	- package URL; `"peterlavalle.nug": "https://gitlab.com/peterlavalle/peters-unity-stuff.git?path=/workspace-nug.unity/Packages/peterlavalle.nug"`



