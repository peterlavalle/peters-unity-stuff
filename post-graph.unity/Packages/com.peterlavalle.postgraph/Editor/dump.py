"""
dumb python script to dump-out some shader code for the

"""
COUNT = 5



print(f"\t\t\t\tuniform int _AccumulationCount; // up to {COUNT}")

for i in range(0, COUNT):
	print(f"\t\t\t\tsampler2D _AccumulationTex{i};")


print()
print()
print()

# blending
for i in range(0, COUNT):
	print(f"\t\t\t\t\tif ({i} < _AccumulationCount)")
	print(f"\t\t\t\t\t\tcol += tex2D(_AccumulationTex{i}, uv);")
print(f"\t\t\t\t\tcol /= ((float)(_AccumulationCount + 1)));")


