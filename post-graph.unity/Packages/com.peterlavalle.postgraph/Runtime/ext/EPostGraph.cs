
using System.Collections.Generic;
using UnityEngine;

namespace postgraph
{

	public static class EPostGraph
	{
		public static string Hex(this int value)
		{
			return value.ToString("X");
		}

		#region list comprehension extensions

		public static void Each<T>(this IEnumerable<T> list, System.Action<T> work)
		{
			foreach (var item in list)
				work(item);
		}
		public static IEnumerable<O> Each<I, O>(this IEnumerable<I> list, System.Func<I, O> work)
		{
			foreach (var item in list)
				yield return work(item);
		}

		public static System.Func<System.Func<O, L, O>, O> Fold<L, O>(this IEnumerable<L> list, O left)
		{
			return (join) =>
			{
				foreach (var item in list)
					left = join(left, item);

				return left;
			};
		}

		#endregion

		#region list (or whatever) conversion extensions
		public static E[] ToArray<E>(this IEnumerable<E> data)
		{
			var list = data.ToList();
			var array = new E[list.Count];
			for (int i = 0; i < array.Length; i++)
				array[i] = list[i];
			return array;
		}

		public static List<E> ToList<E>(this IEnumerable<E> data)
		{
			return new List<E>(data);
		}
		#endregion

		#region component and lifecycle extions
		//! dispose of a Unity Object - mostly fun for null-propogation
		public static void Dispose(this Object self)
		{
			Object.Destroy(self);
		}

		//! get a component - add it if missing
		public static T OptComponent<T>(this Component self) where T : Component
		{
			return self.GetComponent<T>() ?? self.gameObject.AddComponent<T>();
		}

		//! get a component - add it if missing
		public static T OptComponent<T>(this GameObject self) where T : Component
		{
			return self.GetComponent<T>() ?? self.gameObject.AddComponent<T>();
		}
		#endregion

		#region render-texture extensions

		//! create a texture witht he same size/format as self OR return "self" if the size/format is alreayd a match
		public static RenderTexture Sibling(this RenderTexture self, RenderTexture swap, float scale = 1)
		{
			// check if we don't need to change it or anything
			if (null != swap && self.Simmilar(swap, scale))
			{
				return swap;
			}

			swap?.Dispose();
			swap = new RenderTexture(self);

			swap.Release();
			swap.width = (int)(self.width * scale);
			swap.height = (int)(self.height * scale);
			swap.Create();

			return swap;
		}

		//! test if the size/formats match
		public static bool Simmilar(this RenderTexture self, RenderTexture them, float scale = 1)
		{
			return (
				(self.width * scale) == them.width
				&& self.format == them.format
				&& (self.height * scale) == them.height);
		}
		#endregion
	}
}
