
using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{
	//! enum-type base class for the links each node needs to emit
	//! ... because the forms is `f: Flow -> Lookup -> Texture` and C# syntax sucks for that
	//! ... i imagined i'd nbeed more that two subclasses; i guess not
	public abstract class PipeLink
	{
		//! used for guid purposes
		public readonly PipeUnit unit;

		PipeLink(PipeUnit unit)
		{
			this.unit = unit;
		}

		public class FlowStep : PipeLink
		{
			public readonly Func<PipeRead, Texture> code;

			public FlowStep(PipeUnit unit, Func<PipeRead, Texture> code) : base(unit)
			{
				Debug.Assert(null != code, "can't have null-code");
				this.code = code;
			}
		}

		public class SourceFrame : PipeLink
		{
			public SourceFrame(PipeUnit unit) : base(unit)
			{
			}
		}
	}
}
