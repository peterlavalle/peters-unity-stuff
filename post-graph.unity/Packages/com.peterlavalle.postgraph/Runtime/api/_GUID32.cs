
using Unity.VisualScripting;

namespace postgraph
{
	public abstract class _GUID32
	{
		private static int next = 0;
		private static int salt = new object().GetHashCode();
		public static int Next => (next++) ^ salt;

		[DoNotSerialize]
		public readonly int guid32 = Next;
	}
}
