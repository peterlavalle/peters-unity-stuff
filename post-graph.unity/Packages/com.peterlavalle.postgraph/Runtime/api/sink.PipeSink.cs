
using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{
	[UnitCategory("PipeGraph")]
	public class PipeSink : Unit
	{
		#region graph
		/// <summary>
		/// OBLIGATORY - chains execution
		/// </summary>
		[PortLabelHidden]
		[DoNotSerialize]
		public ControlInput inputTrigger;

		/// <summary>
		/// OBLIGATORY - chains execution
		/// </summary>
		[PortLabelHidden]
		[DoNotSerialize]
		public ControlOutput outputTrigger;

		[PortLabelHidden]
		[DoNotSerialize] public ValueInput pipe;

		protected override void Definition()
		{
			pipe = ValueInput(nameof(pipe), (PipeLink)null);

			outputTrigger = ControlOutput(nameof(outputTrigger));

			inputTrigger = ControlInput(nameof(inputTrigger), (flow) =>
			{
				// get the camera and get or add the behaviour and set the pipe
				// ... this will be BADBAD if there's more than one of these on a single camera
				flow.stack.gameObject
					.GetComponent<Camera>()
					.OptComponent<PipeLinkBehaviour>()
					.Link = flow.GetValue<PipeLink>(pipe);

				return outputTrigger;
			});
		}
		#endregion

		#region the behaviour
		private class PipeLinkBehaviour : MonoBehaviour
		{
			private PipeLink pipeLink;

			public PipeLink Link
			{
				set
				{
					workingTextures.Values.Each(Destroy);
					activeResources.Values.Each((resource) => resource());

					workingTextures.Clear();
					activeResources.Clear();


					pipeLink = value;
				}
			}

			private Dictionary<(int, int), RenderTexture> workingTextures = new();
			private Dictionary<(int, int), Action> activeResources = new();

			void OnDestroy()
			{
				Link = null;
			}

			private void OnRenderImage(RenderTexture srcRenderTexture, RenderTexture outRenderTexture)
			{
				Dictionary<int, Texture> ssa = new();
				HashSet<int> guard = new();
				Action<PipeLink> loop = null; loop = (link) =>
				{
					var guid = link.unit.guid32;

					// check if we've done us already
					if (ssa.ContainsKey(guid))
						return;

					// guard against cycles
					if (!guard.Add(guid))
						throw new Exception($"cycle detected with {link.unit.guid32} / {link.GetType()}");


					// start solving!
					switch (link)
					{
						case PipeLink.SourceFrame:
							ssa[guid] = srcRenderTexture;
							Debug.Assert(null != ssa[guid]);
							break;

						case PipeLink.FlowStep step:
							PipeRead read = new(
								(want) =>
								{
									Debug.Assert(null != want, "can't want null");

									// ensure we're in there
									if (!ssa.ContainsKey(want.unit.guid32))
										loop(want);

									//
									var got = ssa[want.unit.guid32];
									Debug.Assert(null != got);
									return got;
								},
								(work) =>
								{
									var key = (guid, work.guid32);

									var value = workingTextures.ContainsKey(key) ? workingTextures[key] : null;
									value = srcRenderTexture.Sibling(value, work.scale);
									workingTextures[key] = value;

									Debug.Assert(null != value);

									return value;
								},
								(resource, cleanup) =>
								{
									var key = (guid, resource);

									Debug.Assert(!activeResources.ContainsKey(key));

									activeResources[key] = cleanup;
								}
							);

							Debug.Assert(null != step.code, "all steps need code");

							var value = step.code(read);
							Debug.Assert(null != value, "musn't have null textures");
							ssa[guid] = value;
							break;

						default:
							throw new Exception($"didn't handle a link {link.GetType()}");
					}
				};

				loop(pipeLink);

				Graphics.Blit(ssa[pipeLink.unit.guid32], outRenderTexture);
			}
		}
		#endregion
	}
}
