
using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{
	public class PipeRead
	{

		public PipeRead(Func<PipeLink, Texture> ssa, Func<PipeUnit.WorkingTexture, RenderTexture> working, Action<int, Action> resource)
		{
			this.ssa = ssa;
			this.working = working;
			this.resource = resource;
		}


		private readonly Func<PipeLink, Texture> ssa;
		public Texture this[PipeLink read]
		{
			get => ssa(read);
		}


		private readonly Func<PipeUnit.WorkingTexture, RenderTexture> working;
		public RenderTexture this[PipeUnit.WorkingTexture key]
		{
			get => working(key);
		}

		internal readonly Action<int, Action> resource;
	}
}
