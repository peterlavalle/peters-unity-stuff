
using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{
	//! delegate/lambda used from pipe-links
	//! ... because the forms is `f: Flow -> Lookup -> Texture` and C# syntax sucks for that
	public delegate PipeLink ProtoPipe(Flow flow);
}
