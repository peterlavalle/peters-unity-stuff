
using System;
using System.Collections.Generic;
using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{

	//! base class for units that will output a pipe (and only that)
	public abstract class PipeUnit : Unit
	{
		[DoNotSerialize]
		public readonly int guid32 = _GUID32.Next;

		[PortLabelHidden]
		[DoNotSerialize] public ValueOutput pipe = null;

		protected WorkingTexture workingTexture(float scale = 1)
		{
			var workingTexture = new CWorkingTexture(scale);

			workingTextures.Each((old) => Debug.Assert(old.guid32 != workingTexture.guid32));
			workingTextures.Add(workingTexture);

			return workingTexture;
		}

		public List<WorkingTexture> workingTextures = new();

		public class WorkingTexture : _GUID32
		{
			public readonly float scale;
			protected WorkingTexture(float scale) : base()
			{
				this.scale = scale;
			}
		}

		private class CWorkingTexture : WorkingTexture
		{
			public CWorkingTexture(float scale) : base(scale) { }
		}

		protected override void Definition()
		{
			Debug.Assert(null == pipe, $"shouldn't pipe start null? {GetType()} # {guid32.Hex()}");
			pipe = null;

			var link = DefinePipe();

			Debug.Assert(null == pipe, "pipe-unit subclasses must leave the pipe variable alone!");

			ValueOutput mine = null;
			mine = pipe = ValueOutput(nameof(pipe), (flow) =>
			{
				Debug.Assert(mine == pipe, "REALLY! leave the pipe variable alone!");
				return link(flow);
			});
		}

		protected abstract ProtoPipe DefinePipe();

		protected ProtoPipe pipeLink(Func<Flow, Func<PipeRead, Texture>> code)
		{
			return (flow) => new PipeLink.FlowStep(this, code(flow));
		}

		public class Resource<T> : _GUID32
		{
			private Func<T> create;
			private Action<T> dispose;
			private T[] data = null;
			public Resource(Func<T> create, Action<T> dispose)
			{
				this.create = create;
				this.dispose = dispose;
			}
			public T this[PipeRead pipeRead]
			{
				get
				{
					if (null != data)
						return data[0];

					var blip = create();

					pipeRead.resource(guid32, () =>
					{
						dispose(blip);
					});

					return (data = new[] { blip })[0];
				}
			}
		}

		protected Resource<T> resource<T>(Func<T> create, Action<T> dispose)
		{
			return new Resource<T>(create, dispose);
		}

		protected ProtoPipe pipeLink(Func<Flow, Action<PipeRead, RenderTexture>> code)
		{
			return pipeLink((flow) =>
			{
				var shared = workingTexture();

				var actual = code(flow);

				return (read) =>
				{
					var work = read[shared];
					actual(read, work);
					return work;
				};
			});
		}

		protected ProtoPipe sourceFrame()
		{
			return (_) => new PipeLink.SourceFrame(this);
		}
	}
}
