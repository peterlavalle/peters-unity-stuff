

using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{
	[UnitCategory("PipeGraph")]
	public class AlphaBlend : PipeUnit
	{
		[DoNotSerialize] public ValueInput under;

		[DoNotSerialize] public ValueInput over;

		protected override ProtoPipe DefinePipe()
		{
			under = ValueInput<PipeLink>(nameof(under));
			over = ValueInput<PipeLink>(nameof(over));

			return pipeLink((flow) =>
			{
				var over = flow.GetValue<PipeLink>(this.over);
				var under = flow.GetValue<PipeLink>(this.under);

				var (material, pass) = PostGraphShaders.Find("AlphaBlend");

				return (read, outTexture) =>
				{
					var srcTexture = read[under];
					var subTexture = read[over];

					material.SetTexture("_MainTex", srcTexture);
					material.SetTexture("_OverTex", subTexture);

					Graphics.Blit(srcTexture, outTexture, material, pass);
				};
			});
		}
	}
}

