
using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{
	[UnitCategory("PipeGraph")]
	public class WebCam : PipeUnit
	{
		[DoNotSerialize] public ValueInput device;

		[DoNotSerialize] public ValueInput resolution;

		[DoNotSerialize] public ValueInput framerate;

		private static bool active = false;

		protected override ProtoPipe DefinePipe()
		{
			device = ValueInput<string>(nameof(device));
			resolution = ValueInput<Vector2>(nameof(resolution));
			framerate = ValueInput(nameof(framerate), 45);

			return pipeLink((flow) =>
			{
				var device = flow.GetValue<string>(this.device);
				var resolution = flow.GetValue<Vector2>(this.resolution);
				var framerate = flow.GetValue<int>(this.framerate);

				var camera = resource(() =>
				{
					{
						var devices = WebCamTexture.devices;
						Debug.Log(
							devices.Fold($"There are {devices.Length} cameras:")((s, d) => s + "\n\t>" + d.name + "<")
						);
					}

					Debug.Assert(!active, "i/Unity can't handle a second WebCam");
					active = true;

					WebCamTexture webCamTexture = new(device, (int)(resolution.x), (int)(resolution.y), framerate);
					webCamTexture.Play();

					Debug.Log($"connected to WebCamTexture(device = \"{device}\") = \"{webCamTexture.deviceName}\"");

					return webCamTexture;
				},
				(camera) =>
				{
					camera.Stop();
					camera.Dispose();

					Debug.Assert(active);
					active = false;
				});

				return (read) => camera[read];
			});
		}
	}
}
