
using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{
	[UnitCategory("PipeGraph")]
	public class TemporalBlur : PipeUnit
	{
		const int COUNT = 5;

		[PortLabelHidden]
		[DoNotSerialize] public ValueInput input;
		[DoNotSerialize] public ValueInput delay;
		[DoNotSerialize] public ValueInput count;
		[DoNotSerialize] public ValueInput scale;

		protected override ProtoPipe DefinePipe()
		{
			input = ValueInput<PipeLink>(nameof(input));
			delay = ValueInput(nameof(delay), 0.25f);
			count = ValueInput(nameof(count), 20);
			scale = ValueInput(nameof(scale), 0.7f);

			return pipeLink((flow) =>
			{
				//--- setup thing that come from parameters
				var input = flow.GetValue<PipeLink>(this.input);
				var delay = new Delay(flow.GetValue<float>(this.delay));

				var scale = flow.GetValue<float>(this.scale);
				if (scale != 1)
					Debug.Log("something to do with scale messes with the buckets");

				//--- create state (SHOULD I BE DOING THIS DIFFERENTLY? probably not if it works for the textures ... maybe)

				float nextSave = 0.0f;

				var savedFrameIndex = 0;

				var savedFrameResourcess = new RenderTexture[flow.GetValue<int>(this.count)].Each(_ => workingTexture(scale)).ToArray();

				var aSwapResource = workingTexture();
				var bSwapResource = workingTexture();

				Debug.Assert(scale > 0 && 1 <= savedFrameResourcess.Length);

				//--- locate resources

				var (material, pass) = PostGraphShaders.Find("TemporalBlending");

				//--- build the actual function
				Texture BlendBucket(PipeRead read)
				{
					// pull-down resources
					var savedFrame = savedFrameResourcess.Each(one => read[one]).ToList();
					var aSwap = read[aSwapResource];
					var bSwap = read[bSwapResource];

					// setup the loop
					var from = read[input];
					var flip = true;
					RenderTexture into = null;

					RenderTexture flipInto() => flip ? bSwap : aSwap;

					float themFactor = 1.0f / ((float)(savedFrameResourcess.Length + 1));
					float mainFactor = themFactor;

					for (var slot = 0; slot < savedFrameResourcess.Length;)
					{
						var bind = 0;
						for (; bind < COUNT && slot < savedFrameResourcess.Length;)
						{
							material.SetTexture("_AccumulationTex" + bind, savedFrame[slot]);
							slot++;
							bind++;
						}

						material.SetInteger("_AccumulationCount", Mathf.Min(bind, COUNT));
						material.SetFloat("_AccumulationFactor", themFactor);
						material.SetFloat("_MainFactor", mainFactor);

						// run the blit
						Graphics.Blit(source: from, dest: (into = flipInto()), mat: material, pass: pass);

						// advance
						mainFactor = 1;
						from = into;
						flip = !flip;
					}

					Debug.Assert(
						null != into,
						$"i suppose this could happen if count=0, count is {count} at this point");

					// save a frame when we should
					if (delay.Tick())
					{
						Graphics.Blit(read[input], savedFrame[savedFrameIndex]);

						savedFrameIndex++;
						savedFrameIndex %= savedFrameResourcess.Length;
					}

					// return the result
					return into;
				};
				return BlendBucket;

			});
		}
	}

	public class Delay
	{
		private readonly float delay;
		public Delay(float delay)
		{
			this.delay = delay;
			nextSave = Time.realtimeSinceStartup;
			Debug.Assert(delay > 0);
		}

		private float nextSave;


		public bool Tick()
		{
			var now = Time.realtimeSinceStartup;
			if (now < nextSave)
				return false;

			nextSave = now + delay;
			return true;
		}
	}
}
