
using Unity.VisualScripting;

namespace postgraph
{
	[UnitCategory("PipeGraph")]
	public class SourceFrame : PipeUnit
	{
		protected override ProtoPipe DefinePipe()
		{
			return sourceFrame();
		}
	}
}
