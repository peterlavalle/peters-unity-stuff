
using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{
	[UnitCategory("PipeGraph")]
	public class StaticTexture : postgraph.PipeUnit
	{
		[PortLabelHidden]
		[DoNotSerialize] public ValueInput texture;
		protected override ProtoPipe DefinePipe()
		{
			texture = ValueInput<Texture>(nameof(texture), null);

			return pipeLink((flow) =>
			{
				var texture = flow.GetValue<Texture>(this.texture);
				return (_) => texture;
			});
		}
	}
}
