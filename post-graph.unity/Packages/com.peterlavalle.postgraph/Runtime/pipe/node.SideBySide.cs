using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{
	[UnitCategory("PipeGraph")]
	public class SideBySide : PipeUnit
	{
		[PortLabelHidden]
		[DoNotSerialize] public ValueInput lValueInput;

		[PortLabelHidden]
		[DoNotSerialize] public ValueInput rValueInput;

		protected override ProtoPipe DefinePipe()
		{
			lValueInput = ValueInput<PipeLink>(nameof(lValueInput));
			rValueInput = ValueInput<PipeLink>(nameof(rValueInput));

			return pipeLink((flow) =>
			{
				var lLink = flow.GetValue<PipeLink>(lValueInput);
				var rLink = flow.GetValue<PipeLink>(rValueInput);

				var (material, pass) = PostGraphShaders.Find("ShareHorizontal");

				return (read, work) =>
				{
					var lTex = read[lLink];
					var rTex = read[rLink];

					material.SetTexture("_RightTex", rTex);
					Graphics.Blit(lTex, work, material, pass);
				};
			});
		}
	}
}
