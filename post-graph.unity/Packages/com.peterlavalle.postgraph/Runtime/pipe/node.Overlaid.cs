

using UnityEngine;
using Unity.VisualScripting;

namespace postgraph
{
	[UnitCategory("PipeGraph")]
	public class Overlaid : PipeUnit
	{
		[DoNotSerialize] public ValueInput under;

		[DoNotSerialize] public ValueInput over;


		[DoNotSerialize] public ValueInput offset;
		[DoNotSerialize] public ValueInput scale;

		protected override ProtoPipe DefinePipe()
		{
			under = ValueInput<PipeLink>(nameof(under));
			over = ValueInput<PipeLink>(nameof(over));

			offset = ValueInput(nameof(offset), Vector2.zero);
			scale = ValueInput(nameof(scale), Vector2.one);

			return pipeLink((flow) =>
			{
				var over = flow.GetValue<PipeLink>(this.over);
				var under = flow.GetValue<PipeLink>(this.under);
				var offset = flow.GetValue<Vector2>(this.offset);
				var scale = flow.GetValue<Vector2>(this.scale);

				var (material, pass) = PostGraphShaders.Find("Overlay");

				// adjust because math is dumb
				offset = -offset;
				scale.x = 1.0f / scale.x;
				scale.y = 1.0f / scale.y;

				return (read, outTexture) =>
				{
					var srcTexture = read[under];
					var subTexture = read[over];

					material.SetTexture("_MainTex", srcTexture);
					material.SetTexture("_OverTex", subTexture);
					material.SetVector("_Offset", offset);
					material.SetVector("_Scale", scale);

					Graphics.Blit(srcTexture, outTexture, material, pass);
				};
			});
		}
	}
}

