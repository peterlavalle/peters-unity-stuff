using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace postgraph
{
	public class PostGraphShaders
	{
		private static Material _material = null;

		public static (Material, int) Find(string name)
		{
			Material material;
			if (_material != null)
				material = _material;
			else
			{
				var shader = Shader.Find("com.peterlavalle.postgraph/PostGraphShaders");
				Debug.Assert(null != shader, "failed to load the shader");
				material = _material = new Material(shader);
			}
			var pass = material.FindPass(name);
			Debug.Assert(pass >= 0, $"Couldn't find pass {name} in shader, got {pass}");

			return (material, pass);
		}
	}
}
