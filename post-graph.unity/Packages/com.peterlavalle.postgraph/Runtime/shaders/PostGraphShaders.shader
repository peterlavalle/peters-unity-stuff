Shader "com.peterlavalle.postgraph/PostGraphShaders"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
	}
	CGINCLUDE
		sampler2D _MainTex;

		#pragma vertex vert

		#include "UnityCG.cginc"

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		v2f vert(appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv = v.uv;
			return o;
		}
		#pragma fragment frag
	ENDCG
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			Name "AlphaBlend"
			CGPROGRAM
				sampler2D _OverTex;

				fixed4 frag(v2f i) : SV_Target
				{
					float2 uv = i.uv;
					fixed4 main = tex2D(_MainTex, uv);
					fixed4 over = tex2D(_OverTex, uv);

					return (main * (1 - over.a)) + (over * over.a);
				}
			ENDCG
		}

		Pass
		{
			Name "Overlay"
			CGPROGRAM
				sampler2D _OverTex;
				uniform float2 _Offset;
				uniform float2 _Scale;

				fixed4 frag(v2f i) : SV_Target
				{
					float2 uv = i.uv;
					fixed4 col = tex2D(_MainTex, uv);

					uv.x *= _Scale.x;
					uv.y *= _Scale.y;

					uv += _Offset;

					if (uv.x < 0)
						return col;
					if (uv.x >= 1)
						return col;
					if (uv.y < 0)
						return col;
					if (uv.y >= 1)
						return col;

					return  tex2D(_OverTex, uv);
				}
			ENDCG
		}

		Pass
		{
			Name "ShareHorizontal"
			CGPROGRAM
				sampler2D _RightTex;
				fixed4 frag(v2f i) : SV_Target
				{
					float2 uv = i.uv;
					uv.x *= 2;
					fixed4 col = tex2D(_MainTex, uv);

					if (uv.x >= 1)
						col = tex2D(_RightTex, uv - float2(1, 0));

					return col;
				}
			ENDCG
		}

		Pass
		{
			Name "TemporalBlending"
			CGPROGRAM
				uniform int _AccumulationCount; // up to 5
				uniform float _AccumulationFactor;
				uniform float _MainFactor;
				uniform sampler2D _AccumulationTex0;
				sampler2D _AccumulationTex1;
				sampler2D _AccumulationTex2;
				sampler2D _AccumulationTex3;
				sampler2D _AccumulationTex4;
				fixed4 frag(v2f i) : SV_Target
				{
					float2 uv = i.uv;

					float factor = _AccumulationCount;
					factor += 1.0;
					factor = 1.0 / factor;

					float4 col = tex2D(_MainTex, uv) * _MainFactor;

					if (0 < _AccumulationCount)
						col += tex2D(_AccumulationTex0, uv) * _AccumulationFactor;
					if (1 < _AccumulationCount)
						col += tex2D(_AccumulationTex1, uv) * _AccumulationFactor;
					if (2 < _AccumulationCount)
						col += tex2D(_AccumulationTex2, uv) * _AccumulationFactor;
					if (3 < _AccumulationCount)
						col += tex2D(_AccumulationTex3, uv) * _AccumulationFactor;
					if (4 < _AccumulationCount)
						col += tex2D(_AccumulationTex4, uv) * _AccumulationFactor;


					return col;
				}
			ENDCG
		}
	}
}
