
https://gitlab.com/peterlavalle/peters-unity-stuff.git?path=/post-graph.unity/Packages/com.peterlavalle.postgraph#hg.default

post-processing as a Unity/Bolt Flow VisualScripting

# Post Graph (or is it Pipe Graph?)

This technically exposes a bunch of things, one of them is a pipe sink which reacts to an event and turns its input value into a post-processing function for whatever camera it's attached to.
The rest of the nodes build up a post-processing function using various shenanigans.

That might be the confusing aspect; this thing builds up a function rather than computes individual pixels or frames.
This has the delightful consequence that the execution of that function doesn't need to dial in and out of the graph. 
This has the unfortunate consequence, that live or script editing the function won't update it - the pipe sync needs to be reinvoked.
Repeatedly reinvoking the pipe sink however isn't a good idea as that will release and dispose of the assets and Resources for the previous function (even if the functions are identical) resulting in things like temporal blur never being able to engage as their backlog of frames will never fill in.

