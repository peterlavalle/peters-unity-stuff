
# Nug / NuGet Unity DLLs


add a file like this ...

```
; call this FaceONNX.ung

flavour = netstandard2.0
version = 2.2.0.4
```

... to your assets.
On Import, Unity will download a `.dll` and put it next to this file.
What's cool abotu that?

Now you don't need to commit those icky binary `.dll` files - just the `.ung` files.

You **do** still **need the `??.dll.meta`** files.

There are other options and stuff [in the script](Packages/peterlavalle.nug/Editor/src/UNuGet.cs) but I'm not sure if anyone else will use this project.
