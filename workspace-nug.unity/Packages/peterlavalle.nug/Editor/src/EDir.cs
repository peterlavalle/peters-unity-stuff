using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class EDir
{
	public static string Cache(this string into, string url)
	{
		return Cache(
			(into.EndsWith("/") ? into : (into + "/")) + url.GetHashCode().ToString("X") + ".nuget",
			() =>
			{
				using var webClient = new System.Net.WebClient();
				return webClient.DownloadData(url);
			}
		);
	}

	public static string Cache(this string path, System.Func<byte[]> reader)
	{
		if (!File.Exists(path))
		{
			CreateParents(path);

			var data = reader();
			using var output = new FileStream(path, FileMode.Create);
			output.Write(data, 0, data.Length);
		}

		return path;
	}

	public static void CreateParents(this string path)
	{
		var parent = Directory.GetParent(path);

		if (!parent.Exists)
			CreateParents(parent.FullName);

		Directory.CreateDirectory(parent.FullName);
	}
}
