
using System.Collections.Generic;

using System.IO;

static class Properties
{

	public static void Opt(this Dictionary<string, string> data, string key, string val)
	{
		if (!data.ContainsKey(key))
			data[key] = val;
	}

	public static void LoadProperties(this Dictionary<string, string> data, string path)
	{
		foreach (var row in File.ReadAllLines(path))
		{
			var line = row.Trim();
			if (line.StartsWith(";") || "" == line)
				continue;

			var key = line.Split('=')[0];


			var val = line.Substring(key.Length + 1);

			key = key.Trim();
			val = val.Trim();

			data[key] = val;
		}
	}
}
