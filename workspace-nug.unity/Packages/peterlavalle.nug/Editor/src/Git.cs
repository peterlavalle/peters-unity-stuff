using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
public class Git
{
	public static void Ignore(string path)
	{
		var into = Directory.GetParent(path).FullName;
		var ignore = into + "/.gitignore";
		var name = path.Substring(path.LastIndexOf('/') + 1);

		if (File.Exists(ignore))
			foreach (var line in File.ReadAllLines(ignore))
				if (line == name)
					return;

		if (!File.Exists(ignore))
			File.WriteAllText(ignore, "# magic git ignore\n");

		File.AppendAllText(ignore, $"\n# added from .ung\n{name}\n");
	}
}
