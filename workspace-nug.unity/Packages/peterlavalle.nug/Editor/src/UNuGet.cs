#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.AssetImporters;
using System.IO;
using System.IO.Compression;
using UnityEditor;

/// <summary>
/// download some DLL from NuGet
/// </summary>
[ScriptedImporter(1, "ung")]
public class UNuGet : ScriptedImporter
{
	public override void OnImportAsset(AssetImportContext context)
	{
		// work out what we should download
		var data = new Dictionary<string, string>();
		{
			data.Add("website", "https://www.nuget.org/api/v2/package/");
			data.Add("flavour", "netstandard2.0");
			data.LoadProperties(context.assetPath);

			var name = context.assetPath;

			name = name.Substring(name.LastIndexOf('/') + 1);
			name = name.Substring(0, name.Length - 4);

			if (!data["website"].EndsWith("/"))
				data["website"] += "/";

			data.Opt("libname", name + ".dll"); // dll name in the zip
			data.Opt("nugname", name); // naem in the url
			data.Opt("fullurl", data["website"] + data["nugname"] + "/" + data["version"]);
		}

		// cache the zip
		var zipFile = "Temp/.ung-cache/".Cache(data["fullurl"]);

		// scan the cached zip and read/dump any/all .dll with "flavour"
		using (ZipArchive zip = ZipFile.Open(zipFile, ZipArchiveMode.Read))
		{
			var entries = new List<ZipArchiveEntry>();
			foreach (ZipArchiveEntry entry in zip.Entries)
				if (entry.FullName.Contains(data["flavour"]))
					if (entry.FullName.EndsWith(data["libname"]))
						entries.Add(entry);

			if (0 == entries.Count)
				throw new System.Exception("no match");

			if (1 != entries.Count)
				throw new System.Exception($"too many matches in {context.assetPath} - be more specific");

			var into = context.assetPath;
			into = into.Substring(0, into.Length - 3) + "dll";
			into.CreateParents();

			// ignore the file
			Git.Ignore(into);

			// write the file
			entries[0].ExtractToFile(into, true);

			// kick the editor
			AssetDatabase.ImportAsset(into);
		}
	}
}

#endif
